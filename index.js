/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import ClientList from './src/views/ClientList'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ClientList);
