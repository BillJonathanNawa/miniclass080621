import React, {useState, useEffect} from 'react'
import {
    View, 
    Text,
    StyleSheet,
    FlatList,
    useWindowDimensions,
    Pressable,
    TextInput,
    Modal,
    Button
} from 'react-native'
import TextCustom from '../components/TextCustom'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Realm from '../databases/Realm'
import realm from '../databases/Realm';

const ClientList = () => {
    const windowWidth = useWindowDimensions().width;
    const windowHeight = useWindowDimensions().height;

    const [modalVisible, setModalVisible] = useState(false)
    const [isUpdate, setIsUpdate] = useState(0)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phoneNumber, setphoneNumber] = useState('')
    const [listClient, setListClient] = useState([
        {
            id:1,
            name:'Ed Sheeran',
            email:'edsheeran@mail.com',
            phoneNumber:'081234567890'
        },
        {
            id:2,
            name:'Taylor',
            email:'Taylor@mail.com',
            phoneNumber:'081234567890'
        }
    ])

    useEffect(()=>{
        getDataStorage()
    },[])

    const renderClientList = ({item})=>(
        <Pressable onPress={()=>setUpdateClient(item)}>
            <View style={{width:windowWidth-50, backgroundColor:'lightgrey', marginVertical:10, padding:20}}>
                <TextCustom>{item.name}</TextCustom>
                <TextCustom style={{fontWeight:'normal'}}>{item.email}</TextCustom>
                <TextCustom style={{fontWeight:'normal'}}>{item.phoneNumber}</TextCustom>
                <Pressable 
                    onPress={()=>deleteClient(item)}
                    style={{position:'absolute', right:15, top:10}}>
                        <TextCustom>x</TextCustom>
                </Pressable>
            </View>
        </Pressable>
    )

    const storeData = async (key, value) => {
        try {
          await AsyncStorage.setItem(key, value)
        } catch (e) {
          // saving error
          console.log(e)
        }
    }

    const storeDataObject = async (key, value) => {
        try {
          const jsonValue = JSON.stringify(value)
          await AsyncStorage.setItem(key, jsonValue)
        } catch (e) {
          // saving error
        }
    }

    const getData = async (key) => {
        try {
          const value = await AsyncStorage.getItem(key)
          if(value !== null) {
            console.log(value)
          }else{
            console.log('No Data')
          }
        } catch(e) {
          // error reading value
          console.log(e)
        }
    }

    const getDataObject = async (key) => {
        try {
          const jsonValue = await AsyncStorage.getItem(key)
          const hasil = jsonValue != null ? JSON.parse(jsonValue) : null;
          console.log(hasil)
        } catch(e) {
          // error reading value
        }
    }
    const SaveButton = ()=>{
        if(isUpdate > 0){
            Realm.write(()=>{
                const UpdateDataClient = realm.objects('Client').filtered('id == ' + isUpdate)[0]
                UpdateDataClient.name = name
                UpdateDataClient.email = email
                UpdateDataClient.phoneNumber = phoneNumber

                console.log('Data Updated')
                getDataStorage()
            })
        }else{
            // storeData('name', name)
            const simpanData = {
                name:name,
                phoneNumber:phoneNumber,
                email:email
            }

            storeDataObject('dataObject', simpanData)

            // REALM
            Realm.write(()=>{
                const LastDataClient = realm.objects('Client').sorted('id',true)[0]
                const newId = LastDataClient == null? 1 : LastDataClient.id + 1 

                Realm.create('Client',{
                    id: newId,
                    name: name,
                    email: email,
                    phoneNumber: phoneNumber
                })

                console.log('Data Added')
            })
        }

        setName('')
        setphoneNumber('')
        setEmail('')
        setModalVisible(!modalVisible)
    }

    const getDataStorage = () => {
        const newList = realm.objects('Client')
        setListClient(newList)
    }

    const deleteClient = (item)=>{
        realm.write(()=>{
            const dataClient = realm.objects('Client').filtered('id =='+ item.id)
            // console.log(dataClient)
            realm.delete(dataClient)
            
            getDataStorage()
        })
    }

    const setUpdateClient = (item)=>{
        setName(item.name)
        setphoneNumber(item.phoneNumber)
        setEmail(item.email)
        setIsUpdate(item.id)

        setModalVisible(true);
    }

    return (
        <View style={{flex:1}}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'rgba(0,0,0,0.3)'}}>
                    <View style={{width:windowWidth*80/100, backgroundColor:'white'}}>
                        
                        <TextCustom>Name</TextCustom>
                        <TextInput 
                            style={styles.input}
                            value={name}
                            onChangeText={(text)=>setName(text)}></TextInput>
                        
                        <TextCustom>Email</TextCustom>
                        <TextInput 
                            style={styles.input}
                            value={email}
                            onChangeText={(text)=>setEmail(text)}></TextInput>
                        
                        <TextCustom>PhoneNumber</TextCustom>
                        <TextInput 
                            style={styles.input}
                            value={phoneNumber}
                            onChangeText={(text)=>setphoneNumber(text)}></TextInput>
                        
                        <Button title={isUpdate?'Update':'Add'} onPress={SaveButton}>
                        </Button>
                    </View>

                </View>        
            </Modal>
            {/* Header */}
            <View style={styles.headerContainer}>
                <TextCustom style={{fontSize:24}}>Client List</TextCustom>
            </View>

            {/* Content */}
            <FlatList
                data={listClient}
                renderItem={renderClientList}
                keyExtractor={item => item.id}
                contentContainerStyle={{alignItems:'center', paddingVertical:20}}
            />

            <Button title='check storage' onPress={getDataStorage}></Button>

            <Pressable 
                style={styles.absoluteButton} 
                onPress={()=>{
                    setIsUpdate(0)
                    setModalVisible(true)
                }}>
                <View style={styles.buttonAdd}>
                    <Text style={{fontSize:40, padding:0}}>+</Text>
                </View>
            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    headerContainer:{
        height:80,
        backgroundColor:'#6dc0c7',
        justifyContent:'center',
        alignItems:'center'
    },
    buttonAdd:{
        width:60, 
        height:60, 
        backgroundColor:'#6dc0c7', 
        justifyContent: 'center',
        alignItems:'center',
        borderRadius:30,
    },
    absoluteButton:{
        position:'absolute',
        bottom:20,
        right:20
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
})

export default ClientList
