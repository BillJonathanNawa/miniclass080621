const ClientSchema = {
    name: "Client",
    properties: {
      id: "int",
      name: "string",
      email: "string?",
      phoneNumber: "string?"
    },
    primaryKey: "id",
};

export {ClientSchema}