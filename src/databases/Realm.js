import Realm from 'realm'
import {ClientSchema} from './Schemas'

const realm = new Realm({
    path: "myrealm",
    schema: [ClientSchema],
});

export default realm