import React from 'react'
import {Text, StyleSheet} from 'react-native'

const TextCustom = (props) => {
    return (
        <Text style={[styles.textCustom, props.style]}>
            {props.children}
        </Text>
    )
}

const styles = StyleSheet.create({
    textCustom : {
        fontSize:18,
        fontWeight:'bold'
    }
})

export default TextCustom
